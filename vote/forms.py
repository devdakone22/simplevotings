from django import forms
from django.contrib.auth.models import User
from django.forms import formset_factory

from vote.models import Voting, VotingItem, Profile, FactOfVoting, Report


class NewReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = [
            'text',
        ]


class NewVotingForm(forms.ModelForm):
    """
    Класс формы для создания голосования
    """

    class Meta:
        model = Voting  # По образу Voting
        fields = [
            # В форме следующие поля от Voting:
            'title',
            'maximum_votes',
            # 'author_visible',
            # 'voting_users_visible',
            # 'result_visible',
            'text',
        ]


class VotingItemForm(forms.Form):
    text = forms.CharField(
        label="Текст варианта",
        required=True,
        widget=forms.TextInput(
            attrs={
                'required': True,
                'class': 'form-control',
                'placeholder': 'Текст варианта'
            }
        ),
    )

    class Meta:
        model = VotingItem
        fields = ('text',)


VotingItemFormset = formset_factory(VotingItemForm, extra=0)


#   form = CreatingForm()
#   for i in range(n):
#       form.fields['var_' + str(i)] = forms.CharField(
#       label='Вариант ' + str(i + 1),
#       required=True,
#       max_length=VotingItem._meta._forward_fields_map['text'].max_length,
#       )
#  return form

class VoteForm(forms.Form):
    pass


def get_vote_form(voting, user=None):
    form = VoteForm()

    voting_items = VotingItem.objects.filter(voting=voting)
    is_checked = [False for item in voting_items]
    if user:
        if user.is_authenticated:
            is_checked = [bool(FactOfVoting.objects.filter(user=user, voting_item=item)) for item in voting_items]
    already_voted = any(is_checked)
    type = voting_items[0].type

    choices = []
    for index, item in enumerate(voting_items):
        choices.append(("option{}".format(index), item.text))

    if type == 0:
        if not user.is_authenticated:
            form.fields['vote'] = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={'disabled': True}),
                                                    choices=choices)
        elif not already_voted:
            form.fields['vote'] = forms.ChoiceField(required=True, widget=forms.RadioSelect, choices=choices)
        else:
            form.fields['vote'] = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={'disabled': True}),
                                                    choices=choices, initial="option{}".format(is_checked.index(True)))
    elif type == 1:
        if not user.is_authenticated:
            form.fields['vote'] = forms.MultipleChoiceField(required=True, widget=forms.CheckboxSelectMultiple(
                attrs={'disabled': True}), choices=choices)
        elif not already_voted:
            form.fields['vote'] = forms.MultipleChoiceField(
                required=True, widget=forms.CheckboxSelectMultiple, choices=choices,
                initial=["option{}".format(i) for i in range(len(is_checked)) if is_checked[i]])
        else:
            form.fields['vote'] = forms.MultipleChoiceField(required=True, widget=forms.CheckboxSelectMultiple(
                attrs={'disabled': True}), choices=choices, initial=["option{}".format(i) for i in
                                                                     range(len(is_checked)) if is_checked[i]])
    else:
        raise TypeError
    return form


class RegistrationForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email',)


class AvatarForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('avatar',)


class BiographyForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('biography',)


class ProfileForm(forms.ModelForm):
    avatar = forms.ImageField(label="Аватар", required=False)
    biography = forms.CharField(label="О себе:", max_length=500, widget=forms.Textarea(attrs={"rows": 10, "cols": 20}),
                                required=False)

    class Meta:
        model = Profile
        fields = ('avatar', 'biography',)


class UserNameForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name',)
