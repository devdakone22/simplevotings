from django import template

register = template.Library()


@register.filter
def contains(value, arg):
    if value == arg:
        return True
    return value in arg


@register.filter
def str_add(value, arg):
    return str(value) + str(arg)
