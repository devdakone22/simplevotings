import os
from django import template

register = template.Library()


@register.inclusion_tag(os.path.join('base', 'icon-tag.html'))
def icon(icon_name, size=32, id='', cls='', style=''):
    icon_path = os.path.join(r'\static', 'images', 'icons', icon_name + '.svg')

    return {
        'path': icon_path,
        'size': size,
        'id': id,
        'class': cls,
        'style': style,
    }
