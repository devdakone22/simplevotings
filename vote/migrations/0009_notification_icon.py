# Generated by Django 3.0.2 on 2020-01-31 21:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vote', '0008_auto_20200131_1946'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='icon',
            field=models.CharField(blank=True, default='', max_length=150, verbose_name='Иконка'),
        ),
    ]
